# Create app using cli

argocd get list

argocd app create solar-system-app-2 \
--repo https://gitlab.com/iamimran1996/gitops-argocd.git \
--path ./solar-system \
--dest-namespace solar-system \
--dest-server https://kubernetes.default.svc

# Sync the app
argocd app sync solar-system-app

# Get Project
argocd proj list
# argocd proj get <name>


# edit config map of argocd for reconsiliation

kubectl -n argocd describe pod argocd-repo-server | grip -i "ARGO_RECONSILIATION_TIMEOUT:" -B1

kubectl -n argocd patch configmap argocd-cm --patch='{"data": {"timeout_reconsiliation": "300s"}}'
kubeclt -n argocd rollout restart deploy argocd-repo-server


kubectl -n argocd describe pod argocd-repo-server | grip -i "ARGO_RECONSILIATION_TIMEOUT:" -B1